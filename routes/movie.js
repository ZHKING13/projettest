const express = require('express');
const { createMovie, updateMovie, deleteMovie, getAllMovies, getMovieById } = require('../controllers/movie');
const { isAuthenticated } = require('../middlewares/auth');
const router = express.Router();

// ecrire dans la BD
router.route('/addMovie').post(isAuthenticated, createMovie);




// lire dans la BD
router.route('/movies').get(isAuthenticated, getAllMovies);
router.route('/movie/:id').get( getMovieById);


// mettre à jour dans la BD
 router.route('/update/:id').put(isAuthenticated, updateMovie);

// supprimer dans la BD
router.route('/deleteMovie/:id').delete(isAuthenticated, deleteMovie);
 
 


module.exports = router;