const express = require('express');
const { register, logout, login, userStats, getAllUsers } = require('../controllers/user');
const { isAuthenticated } = require('../middlewares/auth');
const router = express.Router();

// requete pour ecrire dans la BD
router.route('/register').post( register)
router.route('/logout').post(logout)
router.route('/login').post(login)

// requete pour lire dans la BD
router.route('/users').get(isAuthenticated, getAllUsers)
router.route('/users/stats').get(isAuthenticated ,userStats)


module.exports = router; 