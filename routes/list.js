const express = require('express');
const { createList, getList, deleteList, updateList } = require('../controllers/list');
const { isAuthenticated } = require('../middlewares/auth');
const router = express.Router();

// ecrire dans la BD
router.route('/addListe').post(isAuthenticated,createList);

// lire dans la BD
router.route('/getListe').get(getList);

// supprimer dans la BD
router.route('/deleteListe/:id').delete(isAuthenticated, deleteList);
// mettre à jour dans la BD
router.route('/updateListe/:id').put(isAuthenticated, updateList);


module.exports = router; 