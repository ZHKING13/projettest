const List = require('../models/List');

// creer une liste en tant qu'admin
exports.createList = async (req, res, next) => {
  if (req.user.isAdmin) {
    const newList = new List(req.body);
    try {
      const savedList = await newList.save();
      res.status(201).json(savedList);
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json({
      succes: false,
      message: "your are not allowed to do that"
    });
  }
}
// supprimer une liste en tant qu'admin
exports.deleteList = async (req, res, next) => {
  if (req.user.isAdmin) {
    try {
      await List.findByIdAndDelete(req.params.id);
      res.status(201).json('The list has been delete...');
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json({
      succes: false,
      message: "your are not allowed to do that"
    });
  }
}
// obtenir les listes par genre ou type
exports.getList = async (req, res, next) => {
  const typeQuery = req.query.type;
  const genreQuery = req.query.genre;
  let list = [];
  try {
    if (typeQuery) {
      if (genreQuery) {
        list = await List.aggregate([
          { $sample: { size: 8 } },
          { $match: { type: typeQuery, genre: genreQuery } },
        ]);
      } else {
        list = await List.aggregate([
          { $sample: { size: 8 } },
          { $match: { type: typeQuery } },
        ]);
      }
    } else {
      list = await List.aggregate([{ $sample: { size: 8 } }]);
    }
    res.status(200).json(list);
  } catch (err) {
    res.status(500).json(err);
  }
}

// mettre à jour une liste 
exports.updateList = async (req, res, next) => {
  if (req.user.isAdmin) {
    try {
      const updatedList = await List.findByIdAndUpdate(req.params.id, {
        $set: req.body
      }, { new: true });
      res.status(200).json(updatedList);
    } catch (err) {
      res.status(500).json(err);
    }
  } else { 
    res.status(403).json({
      succes: false,
      message: "your are not allowed to do that"
    });
  }
}