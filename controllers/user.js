const User = require('../models/Users');


// enregistrement d'un utilisateur
exports.register = async (req, res) => {
  try {
    const { userName, email, password } = req.body;
    let user = await User.findOne({ email });
    if (user) return res.status(400).json({ succes: false, message: 'user already existe' });
    user = await User.create({ userName, email, password });
    const token = await user.generateToken();

    const option = { expires: new Date(Date.now() + 24 * 60 * 60 * 1000), httpOnly: true }
    res.status(200).cookie("token", token, option).json({
      succes: true,
      user,
      token
    })
  } catch (error) {
    res.status(500).json({
      succes: false,
      message: error.message

    })
  }
}

// connexion d'un utilisateur
exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email }).select("+password");
    if (!user) {
      return res.status(400).json({
        succes: false,
        message: "user does not exist"
      });
    }
    const isMatch = await user.matchPassword(password);
    if (!isMatch) {
      return res.status(400).json({
        succes: false,
        message: "incorect password"
      })
    }
    const token = await user.generateToken();

    const option = { expires: new Date(Date.now() + 24 * 60 * 60 * 1000), httpOnly: true }
    res.status(200).cookie("token", token, option).json({
      succes: true,
      user,
      token
    })


  } catch (error) {
    res.status(500).json({
      succes: false,
      message: error.message

    })
  }
}
// mettre a jour les informations d'un utilisateur
exports.updateProfil = async (req, res) => {
  try {
    const user = await User.findById(req.user._id);
    const { userName, email } = req.body;
    if (userName) {
      user.userName = userName;
    }
   
   
    if (email) {
      user.email = email;
    }
     
    await user.save();
    res.status(200).json({
      succes: true,
      message: "profil update"

    })
  } catch (error) {
    res.status(500).json({
      succes: false,
      message: error.message

    })
  }
}


// deconnexion d'un utilisateur
exports.logout = async (req, res) => {
  try {
    res.status(200).cookie("token", null, { expires: new Date(Date.now()), httpOnly: true }).json({
      succes: true,
      message: "user logged out"
    })
  } catch (error) {
    res.status(500).json({
      succes: false,
      message: error.message

    })
  }
}

exports.deletUser = async (req, res) => {
  if (req.user.id === req.params.id || req.user.isAdmin) {
    try {
      await User.findByIdAndDelete(req.params.id);
      res.status(200).json('user has been deleted...');
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json('You can delete only your account!');
  }
}

exports.userStats = async (req, res) => {
  const today = new Date();
  const lastYear = today.setFullYear(today.setFullYear() - 1);



  try {
    const data = await User.aggregate([
      {
        $project: {
          month: { $month: '$createdAt' },
        },
      },
      {
        $group: {
          _id: '$month',
          total: { $sum: 1 },
        },
      },
    ]);
    res.status(200).json(data);
  } catch (err) {
    res.status(500).json(err);
  }
}

exports.getAllUsers = async (req, res) => {
  const query = req.query.new;
  if (req.user.isAdmin) {
    try {
      const users = query
        ? await User.find().sort({ _id: -1 }).limit(5)
        : await User.find();
      res.status(200).json(users);
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json('You are not allowed to see all users');
  } 
}
