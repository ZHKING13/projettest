const Movie = require('../models/Movie');

//création d'un film en tant qu'admin
exports.createMovie = async (req, res) => {
  if (req.user.isAdmin) {
    const newMovie = new Movie(req.body);
    try {
      let movie = await newMovie.save();
      res.status(201).json({
        succes: true,
        movie
      })
    } catch (error) {
      res.status(500).json({
        succes: false,
        message: error.message
      })
    }


  } else {
    res.status(403).json({
      succes: false,
      message: "your are not allowed to do that"
    })
  }
}
// mise à jour d'un film en tant qu'admin
exports.updateMovie = async (req, res) => {
  if (req.user.isAdmin) {
    try {
      let movie = await Movie.findByIdAndUpdate(req.params.id, {
        $set: req.body
      }, {
        new: true
      });
      res.status(200).json({
        succes: true,
        movie
      })
    } catch (error) {
      res.status(500).json({
        succes: false,
        message: error.message
      })
    }
  } else {
    res.status(403).json({
      succes: false,
      message: "your are not allowed to do that"
    })
  }
}

// obtenir tous les films
exports.getAllMovies = async (req, res) => {
  try {
    let movies = await Movie.find();
    res.status(200).json(movies.reverse());
  } catch (error) {
    res.status(500).json({
      succes: false,
      message: error.message
    })
  }
}
// obtenir un film par son id
exports.getMovieById = async (req, res) => {
  try {
    let movie = await Movie.findById(req.params.id);
    res.status(200).json({
      succes: true,
      movie
    })
  } catch (error) {
    res.status(500).json({
      succes: false,
      message: error.message
    })
  }
}

// suppression d'un film en tant qu'admin
exports.deleteMovie = async (req, res) => {
  if (req.user.isAdmin) {
    try {
      let movie = await Movie.findByIdAndDelete(req.params.id);
      res.status(200).json({
        succes: true,
        message: "movie deleted successfully"
      })
    } catch (error) {
      res.status(500).json({
        succes: false,
        message: error.message
      })
    }
  } else {
    res.status(403).json({
      succes: false,
      message: "your are not allowed to do that"
    })
  }
}
// obtenir alleatoirement un film
exports.getRandomMovie = async (req, res) => {
  const type = req.query.type;
  let movie;
  try {
    if (type === 'series') {
      movie = await Movie.aggregate([
        { $match: { isSeries: true } },
        { $sample: { size: 1 } },
      ]);
    } else {
      movie = await Movie.aggregate([
        { $match: { isSeries: false } },
        { $sample: { size: 1 } },
      ]);
    }
    res.status(200).json(movie);
  } catch (err) {
    res.status(500).json(err);
  }
}