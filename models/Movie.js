const mongoose = require('mongoose');

const MovieSchema = new mongoose.Schema({
    title: { type: String, required: true },
    year: { type: Number, required: true },
    genre: { type: String, required: true },
     desc: { type: String },
   imgT: { type: String },
   imgSm: { type: String },
   trailer: { type: String },
   video: { type: String },
    limit: { type: Number },
    isSeries: { type: Boolean, default: false },

}, {
  timestamps: true
})

module.exports = mongoose.model('Movie', MovieSchema);