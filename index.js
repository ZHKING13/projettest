const express = require('express');
const app = express();
const path = require('path');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
dotenv.config();

mongoose.connect(process.env.MONGO_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true,

}).then(() => console.log('Connected to MongoDB...')).catch(err => console.log('error during connection'));
 

// middleware
app.use(express.json());
app.use(cookieParser());

// build le frontend par heroku avant de l'heberger
// app.use(express.static(path.join(__dirname, "/client/build")));

// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, '/client/build', 'index.html'));
// });
// importer les  routes
const user = require('./routes/user');
const movie = require('./routes/movie');
const list = require('./routes/list');
app.use('/api/v1', user)
app.use('/api/v1', movie)
app.use('/api/v1', list) 
app.listen(process.env.PORT || 5000, () => {
    console.log(`Server is running on port ${process.env.PORT || 5000}`);
})  